UC Service Cards
================

An online version of the University Center service cards.  Used by staff as a quick reference 'cheat sheet' for commonly asked questions.

Uses html5 application cache for off-line browsing, as wifi and cell tower coverage on campus is spotty.

See humans.txt for more information.